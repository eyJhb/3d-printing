import cadquery as cq
# from cq_warehouse.fastener import HexNut, SocketHeadCapScrew, SetScrew
import cq_warehouse.fastener as cqf


doSimple = False
# doSimple = True
size = "M8-1.25"
MM = 1

# screw = SocketHeadCapScrew(
#     size="M2-0.4", fastener_type="iso4762", length=16, simple=doSimple
# )

hexScrew = cqf.HexHeadScrew(
    size=size, fastener_type="iso4017", length=35 * MM, simple=doSimple
).cq_object

hexNut = cqf.HexNut(
    size=size, fastener_type="iso4032", simple=doSimple
).cq_object


cq.exporters.export(hexNut, "stls/hexnut.stl")
cq.exporters.export(hexScrew, "stls/hexScrew.stl")
# show_object(hexNut, "hexNut")
# show_object(hexScrew, "hexScrew")
