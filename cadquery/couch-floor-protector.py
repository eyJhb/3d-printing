import cadquery as cq

outer_width = 52
inner_width = 48.5

base_height = 1.5
inner_height = 3

base = (cq.Workplane("XY")
        .circle(outer_width / 2)
        .extrude(base_height)
)

inner_circle = (cq.Workplane("XY")
                .circle(inner_width / 2)
                .extrude(base_height + inner_height)
                .edges(">Z")
                .chamfer(inner_height * (2/4))
)

f = base.add(inner_circle)
show_object(f)
