import cadquery as cq

# stair triangle at posts
#       65.5    90*
#  40*  ---------
#       \       |
#        \      |
#         \     | 55
#          \    |
#           \   |
#       85   \  |
#             \ |
#              \|
#               50*


wall_thickness = 1
length = 85
depth = 10

height = 55 / 4
height = 3

base = (
    cq.Workplane("XY")
    .rect(length, depth, centered=False)
    .extrude(wall_thickness, both=False)
    # .rotate((0, 0, 0), (0, 1, 0), -40)
)

base2 = (
    cq.Workplane("XY")
    .rect(depth / 2, depth, centered=False)
    .extrude(-5 * wall_thickness, both=False)
    .rotate((0, 1, 0), (0, 0, 0), -140 + 180)
)
