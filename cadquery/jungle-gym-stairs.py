import cadquery as cq

# stair triangle at posts
#       65.5    90*
#  40*  ---------
#       \       |
#        \      |
#         \     | 55
#          \    |
#           \   |
#       85   \  |
#             \ |
#              \|
#               50*


wall_thickness = 3
length = 85
depth = 10

height = 55 / 4
height = 3

base = (
    cq.Workplane("XY")
    .rect(length, depth, centered=False)
    .extrude(wall_thickness, both=False)
    .edges(">Z and |Y").fillet(1)
)
base2 = (
    cq.Workplane("XZ")
    .rect(length, height, centered=False)
    .extrude(wall_thickness, both=False)
    .translate((0, wall_thickness, wall_thickness))
)

grip = (
    cq.Workplane("XZ")
    .rect(wall_thickness, height, centered=False)
    .extrude(wall_thickness * 2, both=False)
    .translate((-wall_thickness, wall_thickness, wall_thickness))
    # .translate((0, wall_thickness, wall_thickness))
)

grip2 = (
    cq.Workplane("XZ")
    .rect(wall_thickness, height, centered=False)
    .extrude(wall_thickness * 2, both=False)
    .translate((length, wall_thickness, wall_thickness))
    # .translate((0, wall_thickness, wall_thickness))
)
# base2 = cq.Workplane("XZ").rect(length, depth).extrude(wall_thickness)

r = (base
     .union(base2)
     .union(grip)
     .union(grip2)
     .edges(">X").edges(">Y").fillet(2)
     .edges("<X").edges(">Y").fillet(2)
     # .edges("|Y").edges("<<Z[1]")
     # .edges("<Z[2]")
     )

# r = base
show_object(r)
