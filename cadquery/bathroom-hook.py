import cadquery as cq
from cadquery import exporters

grip_length = 15
wall_thickness = 3.5
hook_height = 22
hook_gap = 16.1
lip_height = 10
lip_depth = 15

width = 7.5

# grip_length
# |   ___________
# |   |  _____  |
# |-> |  |   |  | <-- holder_height
#     |__|   |  |
#            |  |  |--- lip_depth
#            |  |  |  ____
#            |  |  v  |  |
#            |  |_____|  | <-- lip_height
#            |___________|
#                  ^
#                  |
#                  |-- hook_gap


base = (
    cq.Workplane("XZ")
    .move(0, hook_height - grip_length)
    .line(0, grip_length)
    .line(hook_gap + 2 * wall_thickness, 0)
    .line(0, -hook_height + 2)
    # .lineTo(hook_gap + 2 * wall_thickness + lip_depth, 0)
    .lineTo(hook_gap + 2 * wall_thickness + lip_depth / 2, 0)
    .lineTo(hook_gap + 2 * wall_thickness + lip_depth, 0)
    .line(0, lip_height)
    # .polarLine(10, 45)
    # make outer thickness
    .line(wall_thickness, 0)
    # make angled lip
    .line(0, -lip_height)
    .lineTo((2 * wall_thickness + hook_gap + lip_depth), -wall_thickness)
    .line(-lip_depth - wall_thickness, 0)
    .line(0, hook_height)
    .line(-hook_gap, 0)
    .line(0, -grip_length)
    .line(-wall_thickness, 0)
    .close()
    .extrude(width)
    .edges("|Y")
    .fillet(1.7)
    .edges("|X or |Z")
    .fillet(0.2)
    .consolidateWires()
)

# base = (
#     cq.Workplane("XZ")
#     .move(0, hook_height - grip_length)
#     .line(0, grip_length)
#     .line(hook_gap + 2 * wall_thickness, 0)
#     .line(0, -hook_height)
#     .line(lip_depth, 0)
#     .line(0, lip_height)
#     # .polarLine(10, 45)
#     # make outer thickness
#     .line(wall_thickness, 0)
#     # make angled lip
#     .line(0, -lip_height)
#     .lineTo((2 * wall_thickness + hook_gap + lip_depth), -wall_thickness)
#     .line(-lip_depth - wall_thickness, 0)
#     .line(0, hook_height)
#     .line(-hook_gap, 0)
#     .line(0, -grip_length)
#     .line(-wall_thickness, 0)
#     .close()
#     .extrude(width)
#     .edges("|Y")
#     .fillet(1.7)
#     .edges("|X or |Z")
#     .fillet(0.2)
#     # .consolidateWires()
# )

exporters.export(base, "/home/eyjhb//bathroom-hook.stl")
