import cadquery as cq

wall_thickness = 4

# overlap of the cylinder and square
overlap = 30
# the transition length between cylinder and square
transition = overlap*1.5

circle_radius = 155/2

square_width = 115 - 2*wall_thickness
square_length = 165 - 2*wall_thickness
#    v---- width
# |----|
# |    |
# |    | <--- length
# |----| 



# make tube from cooler to start of fan
path = (cq.Workplane("XZ")
        # .radiusArc((20,50,0), 225)
        .line(0, transition)
)

def makeSweep(swidth, slength, lradius):
    # print(lradius-10)
    defaultSweep = (
        cq.Workplane("XZ")
        # first circle
        .circle(lradius, forConstruction=True)
        .rotateAboutCenter((1, 0, 0), 90)

        # next rect
        .workplane()
        .move(0, transition)
        .rect(swidth, slength, forConstruction=True)
        .rotateAboutCenter((1, 0, 0), 90)

        # .consolidateWires()
        .sweep(path, multisection=True)
    )

    return defaultSweep

s1 = makeSweep(square_width + 2*wall_thickness, square_length + 2*wall_thickness, circle_radius + wall_thickness)
s2 = makeSweep(square_width, square_length, circle_radius)
fs = s1.cut(s2)


squareConnector = (cq.Workplane("XY")
                   .box(square_width, square_length, overlap)
                   .faces("|Z")
                   .shell(wall_thickness, kind="intersection")
                   .translate((0,0,overlap/2+transition))
)


cylinderConnector = (cq.Workplane("XY")
                     .cylinder(overlap, circle_radius)
                     .faces("|Z")
                     .shell(wall_thickness, kind="intersection")
                     .translate((0, 0, -(overlap/2)))
)

# show_object(fs, "final sweep")
# show_object(squareConnector, "square connector")
# show_object(cylinderConnector, "cylinder connector")

r = fs.add(squareConnector).add(cylinderConnector)
show_object(r, "final")
