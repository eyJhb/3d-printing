import cadquery as cq

plug_radius = 10.75 / 2
plug_height = 60.30
slit_width = 1.75  # slit in the middle to allow inserting
slit_height = 35.5


base = (
    cq.Workplane("XY")
    .cylinder(plug_height, plug_radius, centered=False)
    .faces(">Z")
    .fillet(plug_radius)
)

slit = (
    cq.Workplane("XZ")
    .rect(plug_radius * 2, slit_height, centered=False)
    .extrude(slit_width, both=False)
    .translate((0, plug_radius + slit_width / 2, 0))
)

r = base.cut(slit)
show_object(r)
