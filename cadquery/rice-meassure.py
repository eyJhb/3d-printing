import cadquery as cq
import math

portions = 2 # persons I guess
portion_serving = 100 # gram

wall_thickness = 2
radius = 35.0 # radius of container


rice_g = portions * portion_serving
# calculation to get gram of rice to ml `(360/300)*100=116.7`
volume_ml = rice_g * (116.7/100) # in ml (1 ml = 1 cm^3)

# water ml calculation
# https://flawlessfood.co.uk/how-to-cook-basmati-rice/
water_ml = (rice_g / 100) * 125 + 125
water_height_m = (water_ml-volume_ml)/(math.pi * radius**2)
water_height = water_height_m * 1000

water_ring_height = 0.5
water_ring_depth = 0.5

print("water_height", water_height)


height_m = volume_ml/(math.pi * radius**2)
height = height_m * 1000

cylinder = cq.Workplane("XY").cylinder(height, radius+wall_thickness)
cylinder_cut = cq.Workplane("XY").cylinder(height, radius)

base = cq.Workplane("XY").cylinder(wall_thickness, radius+wall_thickness).translate((0, 0, wall_thickness/2))

water_ring = (cq.Workplane("XY")
              .cylinder(water_ring_height, radius)
              .cut(cq.Workplane("XY").cylinder(water_ring_height, radius-water_ring_depth))
              .translate((0, 0, water_ring_height/2 + water_height))
    )

# show_object(water_ring, "water_ring")

fcylinder = (cylinder
            .cut(cylinder_cut)
            .translate((0, 0, height/2+wall_thickness))
            .add(base)
            .add(water_ring)
)

show_object(fcylinder, "fcylinder")
# base = (cq.Workplane("XY")
#         .cylinder(height, radius+wall_thickness)
#         .cut(cq.Workplane("XY").cylinder(height, radius))
#         .translate((0, 0, height/2+wall_thickness))
# )

