import cadquery as cq

outer_diameter = 14
inner_diameter = 6
height = 10


c1 = (cq.Workplane("XY").cylinder(height, outer_diameter/2))
c2 = (cq.Workplane("XY").cylinder(height, inner_diameter/2))

r = c1.cut(c2)
show_object(r)
