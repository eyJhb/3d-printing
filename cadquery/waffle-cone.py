import cadquery as cq

height = 100
diameter = 60
wall_thickness = 2

c1 = (cq.Workplane("XY")
      .lineTo(0, height)
      .lineTo(diameter/2, 0)
      .close()
      .revolve(360)
      # .consolidateWires()
      )

c2 = (cq.Workplane("XY")
      .lineTo(0, height-wall_thickness)
      .lineTo(diameter/2 - wall_thickness, 0)
      .close()
      .revolve(360)
      # .consolidateWires()
      )

r = c1.cut(c2)
show_object(r)
