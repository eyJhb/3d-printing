import cadquery as cq

bs_width = 108
bs_length = bs_width
bs_guiderail_height = 2

arm_width = 30.5
arm_length = arm_width
mount_height = 15
mount_wall_thickness = 5

wall_thickness = 2.5

# build plate
def makePlate(added_side_thickness):
    return (cq.Workplane("XY")
         .rect(bs_width + added_side_thickness, bs_length + added_side_thickness)
         .extrude(wall_thickness + bs_guiderail_height)
         .edges("|Z")
         .fillet(5)
    )

plateBase = makePlate(wall_thickness)
plateRemove = (makePlate(0)
        .translate((0, 0, bs_guiderail_height))
)

plate = plateBase.cut(plateRemove)

# build mount
mountBase = (cq.Workplane("XY")
         .rect(arm_width + mount_wall_thickness, arm_length + mount_wall_thickness)
         .extrude(mount_height + mount_wall_thickness)
)

mountRemove = (cq.Workplane("XY")
               .workplane(mount_wall_thickness/2)
               .rect(arm_width, arm_length*2)
               .extrude(mount_height+mount_wall_thickness, both=False)
               # .translate((0, 0, mount_wall_thickness))
)

mount = mountBase.cut(mountRemove)

show_object(plate, "plate")
show_object(mount, "mount")
