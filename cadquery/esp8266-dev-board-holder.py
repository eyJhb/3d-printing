import cadquery as cq

# ESP8266 holder
wall_thickness = 4
height = 9.4
length = 30
width = 7.5


# CR2032 cable holder
wall_thickness = 2.5
height = 4
length = 15
width = 7.5


result = (
    cq.Workplane("front")
    .line(0, 2 * wall_thickness + height)
    .line(wall_thickness + length, 0)
    .line(0, -wall_thickness)
    .line(-length, 0)
    .line(0, -height)
    .line(length, 0)
    .line(0, -wall_thickness)
    .close()
    .extrude(width)
)
