import cadquery as cq


# parameters
wall_thickness = 2

top_funnel_length = 75
bottom_funnel_length = 25

top_outer_diameter = 75
bottom_outer_diameter = 25

base = (cq.Workplane("XZ")
        .move(top_outer_diameter,0)
        .lineTo(bottom_outer_diameter, top_funnel_length)
        .line(-wall_thickness, 0)
        .lineTo(top_outer_diameter-wall_thickness, 0)
        # .lineTo(top_out)
        .close()
        .revolve()
        .consolidateWires()
)

bottom_part = (cq.Workplane("XZ")
               .move(bottom_outer_diameter, top_funnel_length)
               .line(0, bottom_funnel_length)
               .line(-wall_thickness, 0)
               .line(0, -bottom_funnel_length)
               .close()
               .revolve()

)

f = base.union(bottom_part)

# show_object(bottom_part)
