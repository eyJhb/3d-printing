import cadquery as cq

# path = cq.Workplane("XZ").moveTo(-10, 0).lineTo(10, 0)
path = (cq.Workplane("XZ")
        # .moveTo(-10, 0)
        # .lineTo(10, 0)
        .radiusArc((50,100,0), 125)
)


def test(small_radius, large_radius):
    defaultSweep = (
        cq.Workplane("YZ")
        .workplane()
        # .circle(38.0)
        .circle(small_radius, forConstruction=True)
        .rotateAboutCenter((0, 1, 0), 90)

        .workplane(offset=50.0)
        .move(0,100)
        .circle(large_radius, forConstruction=True)
        .rotateAboutCenter((0, 1, 0), -25)

        .consolidateWires()
        # .combine()
        # .loft()
        # .sweep(path)
        .sweep(path, multisection=True)
    )

    return defaultSweep

outer = test(38/2, 50/2).cut(test(36/2, 36/2))

# show_object(defaultSweep, "test")
