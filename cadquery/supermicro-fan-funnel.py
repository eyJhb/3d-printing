import cadquery as cq

wall_thickness = 4

cpu_cooler_height = 20
cpu_cooler_width = 60 # square
cpu_cooler_overlap = 15


hba_cooler_height = 12
hba_cooler_width = 44 # square

# fans https://noctua.at/en/nf-a6x25-pwm/specification

fan_width = 65
fan_height = fan_width
fan_radius = fan_width/2 - 5
fan_depth = 25

# make tube from cooler to start of fan
path = (cq.Workplane("XZ")
        .radiusArc((20,50,0), 225)
)

def makeTube(swidth, sheight, lradius):
    defaultSweep = (
        cq.Workplane("YZ")
        # first rect
        .circle(lradius, forConstruction=True)
        .rotateAboutCenter((0, 1, 0), 90)

        # next rect
        .workplane(offset=20.0)
        .move(0,50)
        .rect(swidth, sheight, forConstruction=True)
        .rotateAboutCenter((0, 1, 0), -25)

        # .consolidateWires()
        .sweep(path, multisection=True)
    )

    return defaultSweep

t1 = makeTube(cpu_cooler_width, cpu_cooler_height, fan_radius)
t2 = makeTube(cpu_cooler_width+wall_thickness, cpu_cooler_height+wall_thickness, fan_radius+wall_thickness/2)
t3 = t2.cut(t1)

# make tube connection
path2 = (cq.Workplane("XZ")
         .lineTo(0, 10)
         )

def makeSquare(swidth, sheight, lradius):
    defaultSweep = (
        cq.Workplane("YZ")
        # first rect
        .rect(swidth, sheight, forConstruction=True)
        .rotateAboutCenter((0, 1, 0), 90)

        # next rect
        .workplane()
        .move(0,10)
        .circle(lradius, forConstruction=True)
        .rotateAboutCenter((0, 1, 0), 90)

        # .consolidateWires()
        .sweep(path2, multisection=True)
    )

    return defaultSweep

s1 = makeSquare(fan_width, fan_height, fan_radius)
s2 = makeSquare(fan_width+wall_thickness, fan_height+wall_thickness, fan_radius+wall_thickness/2)
s3 = s2.cut(s1)

scut = (s3
        .faces("<Z")
        .workplane()
        .rect(fan_width, fan_height)
        .extrude(fan_depth, combine=False)
)

sfinal = (s3
          .faces("<Z")
          .workplane()
          .rect(fan_width+wall_thickness, fan_height+wall_thickness)
          .extrude(fan_depth)
          .cut(scut)
          .translate((0, 0, -10))
)

# tube extension
tcut1 = (t3
      .faces(">Z")
      .workplane(cpu_cooler_overlap/2, centerOption = "CenterOfBoundBox")
      .rect(cpu_cooler_width/1.5, cpu_cooler_height+wall_thickness)
      .extrude(cpu_cooler_overlap, both=False, combine=False)
)

tcut2 = (t3
      .faces(">Z")
      .workplane(centerOption = "CenterOfBoundBox")
      .rect(cpu_cooler_width, cpu_cooler_height)
      .extrude(cpu_cooler_overlap, both=False, combine=False)
)

tfinal = (t3
      .faces(">Z")
      .workplane(centerOption = "CenterOfBoundBox")
      .rect(cpu_cooler_width+wall_thickness, cpu_cooler_height+wall_thickness)
      .extrude(cpu_cooler_overlap, both=False)
# )
).cut(tcut1).cut(tcut2)

final = sfinal.add(tfinal)


# show_object(path2, "path2")
# show_object(sfinal, "square final")
# show_object(tfinal, "tube_final")
# show_object(t3, "tube 3")
# show_object(tcut1, "tube cut 1")
# show_object(tcut2, "tube cut 2")

show_object(final, "final")
