import cadquery as cq

# # If I have an array of 14 locations, is there then a way that I can place an item/object at all these locations?
# yes, it works very well, here is some example code that might help you
# ```
# pts = [(x*box_maj_wd, y*box_maj_wd) for x in range(0,x_grid_number) for y in range(0,y_grid_number)]
# f2a = f2.pushPoints(pts).eachpoint(lambda loc: f2.val().moved(loc), combine="a") #join all sockets
# ```
# There are built-in methods in CQ to make "rectangular arrays" which is an even simpler way of generating the array pts rather than using a list comprehension as I did above.

# sn (small number) used to fillet
sn = 10**(-4)


# basic information
corner_fillet = 10 
height = 3
# height = 2.75
wall_thickness = 1

# outer length might be about 160mm, not entirely sure
outer_length = 162
inner_length = outer_length - 2*wall_thickness

# width
outer_width = 74
inner_width = outer_width - 2*wall_thickness


# how much the tap to pull the backcover
# is off the backside of the cover
pull_tap_cover_back = 0.5
pull_tap_width = 17
pull_tap_bottom_dist = 22.5

# the distance between each corner, and the taps
# that hold the cover in place
corner_to_tap_distance = 15
tap_distance = 25.5 # or 25.5??

tap_width_base = 4.5 # 4.5 before
tap_width_point = 1.5
tap_depth = 1

# camera stuff
## the distance between the outer edge,
## and where the camera triangle start
outer_edge_to_camera = 6 # should be 7, lower for testing
camera_triangle_side_lengths = 45 # TODO(needs adjusting)
camera_lense_diameter = 12.75
camera_lense_edge_to_cutout = 1

# battery stuff
battery_pos_bottom_up = 22 
battery_pos_from_edge = 4
battery_pos_depth = 0.2
battery_length = 93
battery_width = 49

# makes a basecover, with the add_wl (width length)
# variable added to the length and width
def makeBaseCover(add_wl):
    return (cq.Workplane("front")
            .rect(inner_width + add_wl, inner_length + add_wl, centered=False)
            .extrude(height)
            .edges("|Z")
            .fillet(corner_fillet)
            .edges("<Z")
            .fillet(height - sn)
    )

# make the part that will be used to cut out the inner
# part of the cover
inner_cover_cut = makeBaseCover(0).translate((wall_thickness, wall_thickness, wall_thickness))
cover1 = makeBaseCover(wall_thickness*2).cut(inner_cover_cut)

# make list of points to place taps

# camera triangle
tri = (cq.Workplane("front")
       .lineTo(0, camera_triangle_side_lengths)
       .lineTo(-camera_triangle_side_lengths, camera_triangle_side_lengths)
       .close()
       .extrude(height, both=True)
       .edges("|Z")
       .fillet(7)
       .translate((
           outer_width - outer_edge_to_camera,
           outer_length - camera_triangle_side_lengths - outer_edge_to_camera,
           0
       ))
)

# # battery indent
battery_indent = (cq.Workplane("front")
                  .rect(battery_width, battery_length, centered=False)
                  .extrude(height)
                  .edges("|Z")
                  .fillet(corner_fillet)
                  .edges("<Z")
                  .fillet(height - sn)
                  # .translate((battery_pos_bottom_up -(outer_length)/2, -(outer_width/2) + battery_pos_from_edge, wall_thickness - battery_pos_depth))
                  .translate((
                      outer_width - battery_width - battery_pos_from_edge,
                      battery_pos_bottom_up,
                       wall_thickness - battery_pos_depth
                  ))
)


# taps positions
rss = outer_length / 2
lsw = wall_thickness # left side width
lsh = height - wall_thickness/2 # left side height

rsw = outer_width - wall_thickness - tap_depth # left side width
rsh = lsh # left side height

right_side_dists = [
    corner_to_tap_distance,
    tap_distance,
    32.5,
    tap_distance,
    tap_distance,
]
rsd = []
for i, val in enumerate(right_side_dists):
    if i == 0:
        rsd.append((rsw, val, rsh))
    else:
        rsd.append((rsw, rsd[i-1][1] + tap_width_base + val, rsh))

left_side_dists = [
    corner_to_tap_distance,
    32.5,
    tap_distance,
    tap_distance,
    tap_distance,
]
lsd = []
for i, val in enumerate(left_side_dists):
    if i == 0:
        lsd.append((lsw, val, lsh))
    else:
        lsd.append((lsw, lsd[i-1][1] + tap_width_base + val, lsh))
    print(lsd[i][1])

bsd = [
   (corner_to_tap_distance, wall_thickness, lsh),
   (outer_width - corner_to_tap_distance, wall_thickness, lsh),
]

tsd = [
   (corner_to_tap_distance, outer_length - wall_thickness - tap_depth, lsh),
   (outer_width - corner_to_tap_distance, outer_length - wall_thickness - tap_depth, lsh),
]

# tap = (cq.Workplane("XY").box(4, 8, 16))
# tap = (cq.Workplane("XY").sketch().trapezoid(tap_width_base, tap_depth, 160))
tapLeft = (cq.Workplane("front")
       .lineTo(0, tap_width_base)
       .lineTo(tap_depth,  (tap_width_base - tap_width_point)/2 + tap_width_point)
       .lineTo(tap_depth,  (tap_width_base - tap_width_point)/2)
       .close()
       .extrude(wall_thickness/2)
    )
tapBottom = (cq.Workplane("front")
       .lineTo(tap_width_base, 0)
       .lineTo((tap_width_base - tap_width_point)/2 + tap_width_point, tap_depth)
       .lineTo((tap_width_base - tap_width_point)/2, tap_depth)
       .close()
       .extrude(wall_thickness/2)
    )
tapRight = tapLeft.rotateAboutCenter((0,1,0), 180)
tapTop = tapBottom.rotateAboutCenter((0,0,1), 180)
# tap_width_base = 4.5
# tap_width_point = 4.5
# tap_depth = 1
# show_object(tapBottom)

# coverTaps = cover1.pushPoints(test).eachpoint(lambda loc: tap.located(loc))
coverTaps = cover1.pushPoints(lsd).eachpoint(lambda loc: tapLeft.val().moved(loc), combine="a")
coverTaps = coverTaps.pushPoints(rsd).eachpoint(lambda loc: tapRight.val().moved(loc), combine="a")
coverTaps = coverTaps.pushPoints(bsd).eachpoint(lambda loc: tapBottom.val().moved(loc), combine="a")
coverTaps = coverTaps.pushPoints(tsd).eachpoint(lambda loc: tapTop.val().moved(loc), combine="a")
# coverTaps = cover1.pushPoints(test).eachpoint(lambda loc: tap.moved(loc), combine="a")

# make pull tap
pullTap = (cq.Workplane("front")
           .rect(25, pull_tap_width, centered = False)
           .extrude(10)
           .edges("<Z")
           .fillet(5)
           .translate((-10, pull_tap_bottom_dist, wall_thickness + pull_tap_cover_back))
    )

# test
cameraAlign = (cq.Workplane("front")
               .rect(20, 1, centered = False)
               .extrude(height * 2)
               .translate((outer_width - 20, outer_length - 30 - camera_lense_edge_to_cutout, 0))
        )

show_object(cameraAlign, "align")
show_object(coverTaps.cut(tri).cut(battery_indent).cut(pullTap), "cover1")
