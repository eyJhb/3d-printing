import cadquery as cq

# user changeable
p_series = True # if True there will be made small seperators every other slot
p_batteryLength = 50.0 + 2.0 # add 2.0 because of the battery contact spring
p_batteryDiameter = 14.5

# TODO(eyJhb): fix not working with 1
p_batNum = 2

p_thickness = 2.0

# semi-static variables
p_batteryInnerSep = 1.25 # air seperation at the top and bottom of battery
p_batterySepThick = 1.0 # used for seperation between batteries, and sep between pads/contacts

# automatically generated? Maybe?

## inner battery dimensions
p_innerWidth = p_batNum * (p_batteryDiameter + p_batterySepThick) - p_batterySepThick
p_innerLength = p_batteryLength + 2*(p_batteryInnerSep+p_batterySepThick)
p_innerHeight = p_batteryDiameter

## outer dimensions
# calculate the outer width based on the number of batteries.
# each battery will take up its own diameter + the seperation thickness.
# however the last battery will be against the outer wall, so we have to substract that.
p_outerWidth = p_innerWidth + 2*p_thickness
p_outerLength = p_innerLength + 2*p_thickness
p_outerHeight = p_innerHeight + 2*p_thickness # for the lid as well


# == CONSTRUCTION ==

# basic box
oshell = cq.Workplane("XY").rect(p_outerWidth, p_outerLength).extrude(p_outerHeight + p_thickness)
ishell = (oshell.faces("<Z").workplane(p_thickness, invert=True)
          .rect(p_outerWidth - 2*p_thickness, p_outerLength - 2*p_thickness)
          .extrude(p_outerHeight, combine=False)
)

box = oshell.cut(ishell)

# make each battery box
## TODO(eyJhb): all this could be a tag. The selectors are all the same...?
## initial lines
batteryVSlots = (box.edges("|Z and <<X[-2] and <<Y[-2]").vertices("<Z").workplane()
         .rarray(p_batteryDiameter + p_batterySepThick, 1, p_batNum - 1, 1)
         .rect(p_batterySepThick, p_batteryLength).extrude(p_innerHeight, False)
)

## cut out all the seperators in the middle, so they don't ge all the way up
batteryVSlotsFillet = (box.faces(">Z").workplane()
          .rect(p_outerWidth - 2*p_thickness, p_outerLength / 1.75)
          .extrude(- p_batteryDiameter * 0.40, False)
          .edges("<Z and |X").fillet(2)
)

## create the horizontal lines w/ slots/cutouts for pads
batteryHLines = (box.edges("|Z and <<X[-2] and <<Y[-2]").vertices("<Z").workplane()
          # 2*p_batterysepthick/2 because we extrude from the center
          # writing it just to show it
          .rarray(1, p_batteryLength + 2*(p_batterySepThick/2), 1, 2)
          .rect(p_innerWidth, p_batterySepThick).extrude(p_innerHeight, False)
)

batteryHLinesCuts = (box.edges("|Z and <<X[-2] and <<Y[-2]").vertices("<Z").workplane()
          # 2*p_batterysepthick/2 because we extrude from the center
          # writing it just to show it
          .rarray(p_batteryDiameter + p_batterySepThick, 1, p_batNum, 1)
          .rect(p_batteryDiameter/2, p_innerLength - 2*p_batteryInnerSep).extrude(p_innerHeight, False)
)

# make a little cylinder shape to hold our batteries
batteryHHolderLines = (box.edges("|Z and <<X[-2] and <<Y[-2]").vertices("<Z").workplane()
          # 2*p_batterysepthick/2 because we extrude from the center
          # writing it just to show it
          .rarray(1, p_batteryLength/2 + 2*(p_batterySepThick/2), 1, 2)
          .rect(p_innerWidth, p_batterySepThick).extrude(p_innerHeight/2, False)
)

batteryHHolderLinesCuts = (box.edges("|Z and <<X[-2] and <<Y[-2]").vertices("<Z").workplane()
          # 2*p_batterysepthick/2 because we extrude from the center
          # writing it just to show it
          .rarray(p_batteryDiameter + p_batterySepThick, 1, p_batNum, 1)
          # TODO(eyJhb): this should maybe be a circle/cylinder instead
          .rect(p_batteryDiameter, p_batteryLength).extrude(p_innerHeight/2, False)
          .edges("<Z and |Y").fillet(p_batteryDiameter/2.0001)
)

# add our vertical lines and horizontal lines
batterySlots = (
    batteryVSlots.cut(batteryVSlotsFillet)
    .union(batteryHLines.cut(batteryHLinesCuts))
    .union(batteryHHolderLines.cut(batteryHHolderLinesCuts))
)

# if series, make small blockers every other slot
if p_series:
    batterySeriesVSlots = (box.edges("|Z and <<X[-2] and <<Y[-2]").vertices("<Z").workplane()
            # 2*p_batteryInnersep/2 because we extrude from the center
            # writing it just to show it
            .rarray(p_batteryDiameter + p_batterySepThick, p_batteryLength + 2*p_batterySepThick + 2*p_batteryInnerSep/2, p_batNum - 1, 2)
    )
    batterySeriesVSlots.objects = batterySeriesVSlots.objects[1::2][0::2] + batterySeriesVSlots.objects[0::2][1::2]
    batterySeriesVSlots = batterySeriesVSlots.rect(p_batterySepThick, p_batteryInnerSep).extrude(p_innerHeight, False)

    batterySlots = batterySlots.union(batterySeriesVSlots)
    


# show
# box = box.union(hline.cut(hline2)).add(hline4)
box = box.union(batterySlots)

show_object(box, "box")
# show_object(hline, "hline")
# show_object(hline2, "hline2")
# show_object(hline3, "hline3")
# show_object(batteryHLines, "batteryHlines")
# show_object(batteryHHolderLines, "batteryHHolderlines")
# show_object(batteryHHolderLinesCuts, "batteryHHolderlinesCuts")
# show_object(batteryHLinesCuts, "batteryHlinesCuts")
# show_object(batterySeriesVSlots, "batterySeriesvslots")
