import cadquery as cq

# user defined
length = 60.0/2
width = 30.0/2
height = 11.5
wall_thickness = 1 
tol = 0.25

baseplate_height = 1
strap_width = 0.75

# calculated
inner_calc = lambda t: t + tol
outer_calc = lambda t: t + wall_thickness + tol

outer_height = height+wall_thickness

outer_width = outer_calc(width)
outer_length = outer_calc(length)

inner_width = inner_calc(width)
inner_length = inner_calc(length)

print(outer_width, inner_width)
print(inner_calc(10), inner_calc(20))

outerPart = (cq.Workplane("XY")
      .ellipse(outer_width, outer_length)
      .extrude(outer_height)
)

innerPart = (outerPart
      .faces(">Z")
      .workplane(-height)
      .ellipse(inner_width, inner_length)
      .extrude(outer_height, False)
             .faces("<Z")
             .fillet(width/3)
)

basePlate = (cq.Workplane("XY")
      .ellipse(outer_width*1.3, outer_length*1.3)
      .extrude(baseplate_height)
)

strapCut = (cq.Workplane("XY")
            .rect(strap_width, length*1.2, centered=False)
            .extrude(height*2)
            .translate((-(strap_width/2), 0, height/2))
    )

# b1 = (cq.Workplane("XY")
# b2 = (b1.faces("<Z").workplane(-height+wall_thickness)
#       .ellipse(width+wall_thickness*2, length+wall_thickness*2)
#       .extrude(height+wall_thickness, False)
# )

f = outerPart.cut(innerPart).add(basePlate).cut(strapCut)
show_object(f, "final")
# show_object(strapCut, "inner")

