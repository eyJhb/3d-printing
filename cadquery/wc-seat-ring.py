import cadquery as cq

inner_diameter = 10
outer_diameter = 32
wall_thickness = 1

base = (cq.Workplane("XY")
        .circle(outer_diameter / 2)
        .extrude(wall_thickness)
        )

inner = (cq.Workplane("XY")
        .circle(inner_diameter / 2)
        .extrude(wall_thickness)
        )

r = base.cut(inner)

show_object(r)
