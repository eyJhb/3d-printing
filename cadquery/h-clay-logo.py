import cadquery as cq


width = 7.5
base_thickness = width

border_height = 1
border_thickness = 0.5

letter_height = width*0.80 
letter_width = letter_height*0.8
letter_thickness = 1.00
letter_indent = base_thickness*0.15

base = (cq.Workplane("XY")
     .rect(width, width)
     .extrude(base_thickness)
)

border = (cq.Workplane("XY")
          .box(width, width, border_height)
          .faces("|Z").shell(-border_thickness, kind="intersection")
          .translate((0,0,2))
)

letter = (cq.Workplane("XY")
          .line(0, letter_height)
          .line(letter_thickness, 0)
          .lineTo(letter_thickness, (letter_height/2)+letter_thickness)
          .lineTo(letter_width-letter_thickness, (letter_height/3)+letter_thickness)
          .lineTo(letter_width-letter_thickness, letter_height)
          .line(letter_thickness, 0)
          .line(0, -letter_height)
          .line(-letter_thickness, 0)
          .lineTo(letter_width-letter_thickness, (letter_height/3))
          .lineTo(letter_thickness, (letter_height/2))
          .lineTo(letter_thickness, 0)
          .close()
          .extrude(10, both=False)
          .translate((-letter_width/2, -letter_height/2, base_thickness - letter_indent))
)

# r = base.add(border).cut(letter)
r = base.cut(letter)
show_object(r, "result")
