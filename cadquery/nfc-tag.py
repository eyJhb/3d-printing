import cadquery as cq

nfc_tag_diameter = 22
nfc_tag_height = 1
total_diameter = nfc_tag_diameter + (2*2)
wall_thickness = 1
total_height = 2 * wall_thickness + nfc_tag_height

chip_base = (cq.Workplane("front")
        .circle(total_diameter/2)
        .extrude(total_height)
)
chip_cutout = chip_base.workplane().circle(nfc_tag_diameter/2).extrude(nfc_tag_height/2, both=True, combine=False)

# chip_text = (chip_base
chip_text = (cq.Workplane("front")
             .workplane(1)
             .text("lol", 10, 2, combine=False, halign="center", valign="center", clean=True)
)


hanger_base = (cq.Workplane("front")
            .polygon(3, 0.80*total_diameter)
            .extrude(total_height)
            .translate((total_diameter/2, 0, 0))
)
hanger_cutout = (hanger_base
                 .circle(2)
                 .extrude(20, both=True, combine=False)
                 .translate((1,0,total_height))
)
# hanger_final = hanger_base.cut(hanger_cutout)

final_shape = (chip_base
               .union(hanger_base)
               .cut(chip_cutout)
               .edges(">X").fillet(total_diameter*0.20)
               .edges(">Z or <Z").fillet(total_height/2-10**(-6))
               .cut(hanger_cutout)
)
# final_shape = chip_base.union(hanger_final)

# final_cut = chip_cut.union(polyTest)
# final_cut = final_cut.edges(">X").fillet(total_diameter*0.20)

# show_object(polyTest, "polyTest")
# show_object(chip_cut, "chip_cut")
# show_object(hanger_base, "hanger_base")
# show_object(hanger_cutout, "hanger_cutout")
show_object(final_shape, "final")
# show_object(chip_text, "chip_text")
