import cadquery as cq

# parameters
height = 15

# TODO(eyJhb): fix stepsize not being used
step_size = 0.1

min_inner_diameter = 40
max_inner_diameter = 43

wall_thickness = 2.5

# calculations
outer_diameter = 45 + 2*wall_thickness

base = (cq.Workplane("XY").circle(outer_diameter/2).extrude(height)
        # .faces("<Z").workplane().circle(5).extrude(-30)
)

for i in range(max_inner_diameter-min_inner_diameter+1):
    current_diameter = max_inner_diameter - i
    current_height = (height-wall_thickness) / (max_inner_diameter-min_inner_diameter+1)

    nstep = base.faces(">Z").circle(current_diameter/2.0).extrude(-1*current_height*(i+1), False).edges("<Z").fillet(current_height/2)
    base = base.cut(nstep)

show_object(base, "base")
