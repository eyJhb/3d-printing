from cadquery import cq

outer_diameter = 75
inner_diameter = 69.5
lip_height = 3
total_height = 2 * lip_height

slit_width = 12
slit_length = 60

big_plate = cq.Workplane("XY").circle(outer_diameter / 2).extrude(lip_height)

small_plate = (
    cq.Workplane("XY")
    .circle(inner_diameter / 2)
    .extrude(total_height - lip_height)
    .translate((0, 0, lip_height))
)

base_plate = big_plate.union(small_plate)


cutout_circ1 = (
    cq.Workplane("XY")
    .circle(slit_width / 2)
    .extrude(2 * total_height)
    .translate((0, slit_length / 2 - slit_width / 2, 0))
)

cutout_circ2 = (
    cq.Workplane("XY")
    .circle(slit_width / 2)
    .extrude(2 * total_height)
    .translate((0, -slit_length / 2 + slit_width / 2, 0))
)

cutout = (
    cq.Workplane("XY")
    .rect(slit_width, slit_length - slit_width)
    .extrude(2 * total_height)
    .union(cutout_circ1)
    .union(cutout_circ2)
)


top_cutout = (
    cq.Workplane("XY")
    .rect(outer_diameter, 20)
    .extrude(10)
    .translate((0, inner_diameter / 2, 0))
)

# show_object(top_cutout)
# r = base_plate.cut(cutout)
r = base_plate.cut(cutout).rotate((0, 0, 0), (0, 0, 1), 25).cut(top_cutout)

# show_object(test2)
# show_object(test)
# show_object(cutout)

show_object(r)
