import cadquery as cq

inner_height = 7.5
inner_radius = 3.5 / 2
print(inner_radius)

wall_thickness = 1

base = (cq.Workplane("XY")
        .circle(inner_radius+wall_thickness/2)
        .extrude(inner_height+wall_thickness)
        .edges(">Z").fillet(inner_radius+wall_thickness/2)
        )

cutout = (cq.Workplane("XY")
        .circle(inner_radius)
        .extrude(inner_height)
        )

r = base.cut(cutout)

show_object(r)
