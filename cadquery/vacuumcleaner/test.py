import cadquery as cq

inner_radius = 38/2
# lower_outer_radius = 15
upper_outer_radius = 45.6 / 2 + 2.5

thickness = 2
height = 100

handle = (cq.Workplane("XZ")
          .moveTo(inner_radius, 0)
          .line(0, height)
          .line(upper_outer_radius-inner_radius+thickness, 0)
          .lineTo(inner_radius+thickness, 0)
          .line(-thickness, 0)
          .close()
          .revolve()
          # .consolidateWires()
)
