# import cadquery as cq

# max_outer_diameter = 38
# min_outer_diameter = 36.5
# height = 25

# obj = (cq.Workplane('XY')
#     .circle(max_outer_diameter)
#     .workplane(height).circle(min_outer_diameter)
#     .loft(True, "cut", False)

# )
# show_object(obj, "obj", options={'alpha': 0.5})

import cadquery as cq
import cq_warehouse.extensions

max_outer_diameter = 38
min_outer_diameter = 35.25
height = 35
thickness = 2.5

obj = (
    cq.Workplane("XY")
    .circle(max_outer_diameter/2)
    .workplane(height)
    .circle(min_outer_diameter/2)
    .loft()
)
conical_face = obj.faces("not %Plane").val()
conical_wall = conical_face.thicken(thickness)
show_object(obj, "obj", options={"alpha": 0.5})
show_object(conical_face, "conical_face")
show_object(conical_wall, "conical_wall")
