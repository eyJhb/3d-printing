import cadquery as cq

wall_thickness = 2

# NOSE
## nose parameters
nose_outer_radius = 31.25 / 2 # original is meassured at 31.5
nose_lip_height = 1
nose_lock_end_to_lip = 50
nose_lock_side_length = 9.5 # 9.8w x 8.7l -> printed at 9, endded up 8.5, increased to 10.5 to test
nose_lock_start_to_end = 12
## temp
nose_total_length = nose_lock_end_to_lip+nose_lock_side_length+nose_lock_start_to_end
tmp_nose_lip_length = 1

nose = (cq.Workplane("XZ")
       .move(nose_outer_radius)
       .line(nose_lip_height, 0)
       .line(0, tmp_nose_lip_length)
       .line(-nose_lip_height, 0)
       .line(0, nose_total_length)
       .line(-wall_thickness, 0)
       .line(0, -nose_total_length-tmp_nose_lip_length)
       .close()
       .revolve(360)
)

skewLip = (nose.faces(">Z").workplane(-3.5)
           .rect(50,50)
           .extrude(20, combine=False)
           .rotateAboutCenter((1, 0, 0), 12.52)
)

squareHole = (nose.faces(">Z").workplane(-nose_lock_start_to_end)
        .move(0, nose_lock_start_to_end + wall_thickness)
        .rect(nose_lock_side_length, 20)
        .extrude(-nose_lock_side_length, False)
)

rnose = (nose.cut(skewLip).cut(squareHole)
     .edges(">Z")
     .chamfer(0.45)
)

exit(1)

# SHAFT START
## shaft parameters
shaft_inner_wall_thickness = 2.5
shaft_inner_radius = 45.6 / 2
shaft_locking_hole_width = 8
shaft_locking_hole_height = 3
shaft_locking_hole_start_at = 6
shaft_depth_to_lip = 34.5
shaft_total_depth = 42
shaft_tupe_inner_thickness = 7

# tmp 
shaft_tmp_extra_length = 3

## base object
base = (cq.Workplane("XZ")
       .move(shaft_inner_radius + wall_thickness, 1) # tmp
       .line(-wall_thickness, 0)
       .line(0, shaft_total_depth)
       .line(-shaft_tupe_inner_thickness, 0)
       .line(0, -(shaft_total_depth-shaft_depth_to_lip))
       .line(-shaft_inner_wall_thickness, 0)
       .line(0, shaft_total_depth-shaft_depth_to_lip+shaft_tmp_extra_length)
       .line(shaft_tupe_inner_thickness+shaft_inner_wall_thickness+wall_thickness, 0)
       .line(0, -shaft_total_depth-shaft_tmp_extra_length)

       .close()
       .revolve(360)
       # rotate because I made it the wrong way around...
       .rotateAboutCenter((1, 0, 0), 180)

       # .consolidateWires()
)

## holes
taps = (base.faces(">Z").workplane(-shaft_locking_hole_start_at)
        .rect(shaft_locking_hole_width, shaft_inner_radius*3)
        .extrude(-shaft_locking_hole_height, False)
        .edges("|Y")
        .fillet(1)
)


## final result
rshaft = base.cut(taps)

# TUBE
tube_inner_radius = nose_outer_radius-wall_thickness
tube_upper_outer_radius = shaft_inner_radius + wall_thickness
tube_length = 10

handle = (cq.Workplane("XZ")
          .moveTo(tube_inner_radius, 0)
          .line(0, tube_length)
          .line(tube_upper_outer_radius-tube_inner_radius+wall_thickness, 0)
          .lineTo(tube_inner_radius+wall_thickness, 0)
          .line(-wall_thickness, 0)
          .close()
          .revolve()
          # .consolidateWires()
)


# show_object(rnose, "result-nose", options={"alpha": 0.5})
# show_object(rshaft, "result-shaft", options={"alpha": 0.5})
show_object(handle, "result-handle", options={"alpha": 0.5})
