import cadquery as cq

# result = (cq.Workplane()
#     .cylinder(10, 10, (10,20,0))
#     # .cylinder(10, 10, angle=180)
#     # .radiusArc((10,10,0), 10)
#     # .revolve(180)
#     # .cylinder(10, 10)


#     # .circle(1)
#     # .revolve(90, (-20,0,0),(-20,10,0))
#     # .center(-20,0)
#     # .workplane()
#     # .rect(20,4)
#     # .extrude("next")
# )



# OTHER TEST!
# pts = [
#     (1, 1, 1),
#     (1, 2, 2),
#     (1, 3, 3),
# ]
# p = cq.Workplane("XY").radiusArc((10, 10, 0), 10)
# # p = cq.Workplane("XY").spline(pts, includeCurrent=True)
# s = cq.Workplane("XY").circle(20).sweep(p)

# path = (
#     cq.Workplane("XZ")
#     .moveTo(-5, 4)
#     .lineTo(0, 4)
#     # .threePointArc((4, 0), (0, -4))
#     .lineTo(-5, -4)
#     .consolidateWires()
# )

path = (
    cq.Workplane("XZ")
    # .radiusArc((5,15,0), 20)
    .moveTo(-5, 4)
    .lineTo(0, 4)
    .threePointArc((4, 0), (0, -4))
    .lineTo(-5, -4)
    # .consolidateWires()
)

# test = (
#     cq.Workplane("YX")
#     .workplane(offset=-5)
#     .circle(1.5)
#     # .moveTo(0, 4)
#     .workplane(offset=-10)
#     .circle(1.0)
#     .sweep(path)
#     # .consolidateWires()
# )

# arcSweep = (
#     cq.Workplane("YZ")
#     # .moveTo(0, 10)
#     .circle(1.5)
#     # .sweep(path, multisection=True)
#     .sweep(path)
# )


arcSweep = (
    cq.Workplane("YZ")
    .workplane(offset=-5)
    .moveTo(0, 4)
    .circle(1.5)
    .workplane(offset=5, centerOption="CenterOfMass")
    .circle(1.5)
    .moveTo(0, -8)
    .circle(1.0)
    .workplane(offset=-5, centerOption="CenterOfMass")
    .circle(1.0)
    # .consolidateWires()
    .sweep(path, multisection=True)
)


# NEW TEST!

# path = (cq.Workplane("XZ")
#         .lineTo(0, 20)
#         # .consolidateWires().val()
# )

# test = (cq.Workplane("XY")
#         .workplane(offset=-5)
#         .circle(5)
#         .workplane(offset=-15)
#         .circle(10)
#         # .workplane()
#         .sweep(path, multisection=True)
#         # .consolidateWires()
# )

