import cadquery as cq

# parameters
wall_thickness = 1
inner_wall_thickness = 2.5
inner_radius = 45.6 / 2
locking_hole_width = 8
locking_hole_height = 3
locking_hole_start_at = 6


depth_to_lip = 34.5
total_depth = 42

# inner_tupe_radius = inner_radius - 7
tupe_inner_thickness = 7


# tmp 
tmp_extra_length = 3

# build

## base object
base = (cq.Workplane("XZ")
       .move(inner_radius + wall_thickness, 1) # tmp
       .line(-wall_thickness, 0)
       .line(0, total_depth)
       .line(-tupe_inner_thickness, 0)
       .line(0, -(total_depth-depth_to_lip))
       .line(-inner_wall_thickness, 0)
       .line(0, total_depth-depth_to_lip+tmp_extra_length)
       .line(tupe_inner_thickness+inner_wall_thickness+wall_thickness, 0)
       .line(0, -total_depth-tmp_extra_length)

       .close()
       .revolve(360)
       # rotate because I made it the wrong way around...
       .rotateAboutCenter((1, 0, 0), 180)

       # .consolidateWires()
)

## holes
taps = (base.faces(">Z").workplane(-locking_hole_start_at)
        .rect(locking_hole_width, inner_radius*3)
        .extrude(-locking_hole_height, False)
        .edges("|Y")
        .fillet(1)
)


## final result
r = base.cut(taps)


# show_object(taps, "taps", options={"alpha": 0.5})
# show_object(base, "base", options={"alpha": 0.5})
show_object(r, "result", options={"alpha": 0.5})
