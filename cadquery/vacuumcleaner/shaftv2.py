import cadquery as cq

nose_outer_radius = 31.25 / 2 # original is meassured at 31.5
nose_wall_thickness = 1
nose_lip_height = 1
nose_lock_end_to_lip = 50
nose_lock_side_length = 9.5 # 9.8w x 8.7l -> printed at 9, endded up 8.5, increased to 10.5 to test
nose_lock_start_to_end = 12

# https://cadquery.readthedocs.io/en/latest/classreference.html?highlight=consolidatewire#cadquery.Workplane.consolidateWires

# temp
nose_total_length = nose_lock_end_to_lip+nose_lock_side_length+nose_lock_start_to_end

tmp_lip_length = 2

obj = (cq.Workplane("XZ")
       .move(nose_outer_radius)
       .line(nose_lip_height, 0)
       .line(0, tmp_lip_length)
       .line(-nose_lip_height, 0)
       .line(0, nose_total_length)
       .line(-nose_wall_thickness, 0)
       .line(0, -nose_total_length-tmp_lip_length)
       .close()
       .revolve(360)



       # .consolidateWires()
       # .line(, 50)
)

skewLip = (obj.faces(">Z").workplane(-3.5)
           .rect(50,50)
           .extrude(20, combine=False)
           # .rotate((0, 0, 0), (1, 0, 0), 90)
           # .rotateAboutCenter((1, 0, 0), 12.52)
           .rotateAboutCenter((1, 0, 0), 12.52)
           # .translate((1,0,0))
)

# test = obj.cut(skewLip)

obj2 = (obj.faces(">Z").workplane(-nose_lock_start_to_end)
        # .move(nose_outer_radius, nose_lock_start_to_end)
        .move(0, nose_lock_start_to_end + nose_wall_thickness)
        .rect(nose_lock_side_length, 20)
        .extrude(-nose_lock_side_length, False)
)

# test = obj.cut(obj2)

r = (obj.cut(skewLip).cut(obj2)
     .edges(">Z")
     # .fillet(0.35)
     .chamfer(0.45)
)

# show_object(obj)
# show_object(obj, "obj", options={"alpha": 0.5})
# show_object(test)
# show_object(obj2)
# show_object(r, "result", options={"alpha": 0.5})
show_object(r, "result")
