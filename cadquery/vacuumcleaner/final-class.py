import cadquery as cq

# handle has three parts
# hose attachment
# handle
# nose attachment
class full_handle(object):
    def __init__(self):
        # nose parameters
        self.nose_outer_radius = 31.25 / 2 # original is meassured at 31.5
        self.nose_total_length = 71
        self.nose_lip_height = 1
        ## lock parameters
        self.nose_lock_to_lip = 50 # the side of the lock, that is closet to the handle lip, distance between those two
        self.nose_lock_hole_side_length = 9.5 # 9.8w x 8.7l -> printed at 9, endded up 8.5, increased to 10.5 to test

        # hose parameters
        self.hose_inner_radius = 45.6 / 2
        self.hose_depth_to_inner_lip = 34.5 # worked at 34.5
        self.hose_total_depth = 42
        self.hose_inner_wall_thickness = 2.5
        ## lock parameters
        self.hose_lock_hole_width = 8
        self.hose_lock_hole_height = 3
        self.hose_lock_hole_start_at = 6 # from start of hose/plastic to lock start

        # general
        self.wall_thickness = 2.0
        self.inner_radius = self.nose_outer_radius - self.wall_thickness
        self.blend_extra_length = 1 # extra length for nose/hose going to the handle

    def make_handle(self):
        path = (cq.Workplane("XZ")
                .radiusArc((50,100,0), 125)
        )

        def makeTube(small_radius, large_radius):
            defaultSweep = (
                cq.Workplane("YZ")
                # first circle
                .workplane()
                .circle(small_radius, forConstruction=True)
                .rotateAboutCenter((0, 1, 0), 90)

                # next circle
                .workplane(offset=50.0)
                .move(0,100)
                .circle(large_radius, forConstruction=True)
                .rotateAboutCenter((0, 1, 0), -25)

                # .consolidateWires()
                .sweep(path, multisection=True)
            )

            return defaultSweep

        inner_tube = makeTube(self.inner_radius, self.inner_radius)
        outer_tube = makeTube(self.nose_outer_radius + self.nose_lip_height, self.hose_inner_radius+self.wall_thickness)

        r = outer_tube.cut(inner_tube)
        r.faces(">Z").tag("hose")
        r.faces("<Z").tag("nose")

        return r

    def make_hose(self):
        base = (cq.Workplane("XZ")
                .move(self.hose_inner_radius + self.wall_thickness)
                .line(0, self.hose_depth_to_inner_lip + self.blend_extra_length)
                .line(-self.wall_thickness, 0)
                .line(0, -self.hose_depth_to_inner_lip)
                .line(- (self.hose_inner_radius - self.inner_radius), 0)
                .line(0, -self.blend_extra_length)
                .close()
                .revolve()
        )

        taps = (base.faces(">Z").workplane(-self.hose_lock_hole_start_at)
            .rect(self.hose_lock_hole_width, self.hose_inner_radius * 3)
            .extrude(-self.hose_lock_hole_height, False)
            .edges("|Y")
            .fillet(1)
        )

        r = base.cut(taps)
        r.faces("<Z").tag("Z")

        return r

    def make_nose(self):
        # basic hollow cyllinder
        base = (cq.Workplane("XZ")
            .move(self.nose_outer_radius)
            .line(self.nose_lip_height, 0)
            .line(0, self.blend_extra_length)
            .line(-self.nose_lip_height, 0)
            .line(0, self.nose_total_length)
            .line(-self.wall_thickness, 0)
            .line(0, -self.nose_total_length - self.blend_extra_length)
            .close()
            .revolve()
        )

        # skew in the end of the nose
        skewEnd = (base.faces(">Z").workplane(-4) # original 3.5 with 12.52 angle
                .rect(50,50)
                .extrude(20, combine=False)
                .rotateAboutCenter((1, 0, 0), 15)
        )

        # make the hole for attachments
        ## initially make a square hole, which can the be used for the sloped hole
        squareHole = (base.faces("<Z")
                .workplane(-self.nose_lock_to_lip-self.blend_extra_length)
                .move(0, -self.nose_outer_radius)
                .rect(self.nose_lock_hole_side_length, 20)
                .extrude(-self.nose_lock_hole_side_length, False)
        )

        ## cut the square hole into the base, which means it has a gaping hole in it
        ## this means, that this hole can be used, to only get what we want of the sloped hole
        baseWSquareHole = base.cut(squareHole)

        ## sloped hole, which is made relative to YZ (NOT BASE!! because it would not work), 
        ## it then makes a small square, which it revolves the hole way around, and then everything
        ## from the baseWSquareHole is cut away from it, to only get the exact size hole that is wanted
        slopedHole = (cq.Workplane("YZ")
                       .workplane()
                       .move(self.nose_outer_radius-(self.wall_thickness/1.5), self.blend_extra_length+self.nose_lock_to_lip)
                       .rect(self.wall_thickness/1.5, self.nose_lock_hole_side_length, centered=False)
                       .revolve()
        ).cut(baseWSquareHole)


        r = (base
             .cut(skewEnd)
             .cut(slopedHole)
             .edges(">Z")
             .chamfer(0.8)
        )
        r.faces("<Z").tag("Z")

        return r

    def make_assembly(self):
        rotatedNose = x.make_nose().rotateAboutCenter((0,0,1), 180)
        asm = (cq.Assembly()
               .add(rotatedNose, name="nose")
               .add(self.make_hose(), name="hose")
               .add(self.make_handle(), name="handle")
        )

        cons = (asm
                .constrain("nose@faces@<Z", "handle?nose", "Plane")
                .constrain("hose@faces@<Z", "handle?hose", "Plane")

        )

        return cons.solve()


x = full_handle()

# noseclass = x.make_nose()
# hoseclass = x.make_hose()
# handleclass = x.make_handle()

assembled = x.make_assembly()
show_object(assembled, "assembled")
assembled.save("/tmp/vacuum-complete-handle.step", exportType="STEP")
