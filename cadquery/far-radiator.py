import cadquery as cq


# Side view, where it narrows in on the sides,
# to allow for access to the buttons
# it needs to be rotated 90 degrees, so it will fit over the sides
# |------|
#  )----(
#   )--(
#  )----(
# |------|
#    |-- 22


#      11
#      |
#      |
# |----|
# |----|
#   |-- 22


# side that will cover the radiator display
#  (------------|
# (             |-- 20
# |(------------|
# ||
# ||-- 34
# |-- 43

clip_thickness = 22

wall_thickness = 2
display_arc_start = 34
display_arc_max = 43
display_arc_radius = (display_arc_max - display_arc_start)+1
display_height = 20

button_height = 15
button_radius = (button_height/2)+4

side_angle = 0.5

side1 = (cq.Workplane("XZ")
        .lineTo(display_arc_start+wall_thickness, 0)
        .radiusArc((display_arc_start+wall_thickness, 20), -(display_arc_radius))
        .lineTo(0, display_height)
        .close()
        .extrude(-wall_thickness)
        # .line(0, 5)
    )

side1rot = side1.rotate((0, 0, 0), (0, 0, 1), side_angle)

side2 = side1.translate((-0.2, clip_thickness+wall_thickness, 0))
side2rot = side2.rotate((0, 0, 0), (0, 0, 1), -side_angle)


clip_side = (cq.Workplane("YZ")
        # .line(10,5)
        # .lineTo(wall_thickness, 0)
        .radiusArc((clip_thickness, 0), button_radius)
        .lineTo(clip_thickness, display_height)
        .radiusArc((0, display_height), button_radius)
        .close()
        .extrude(wall_thickness)
        .translate((0, wall_thickness, 00))
)

r = clip_side.union(side1rot).union(side2rot)

# show_object(side1rot)
# show_object(side2rot)
# show_object(clip_side)
show_object(r)
