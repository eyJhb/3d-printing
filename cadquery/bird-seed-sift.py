import cadquery as cq

grit_width = 4
# grit_length = grit_width
grit_length = 4
line_width = 0.5

# radius = 100 / 2 # for bird seed
radius = 160 / 2  # for covering plant pots

grit_height = 1.5

border_width = 2
border_height = 10

# make the cutout area
cutoutArea = (
    cq.Workplane("XY")
    .rect(radius * 4, radius * 4)
    .extrude(grit_height)
    .cut(cq.Workplane("XY").circle(radius).extrude(grit_height))
)

# make actual sift
def mkLines(spacing):
    return (
        cq.Workplane("XY")
        .rarray(spacing + line_width, 1, int(radius / (spacing + line_width)) * 2, 1)
        .rect(line_width, radius * 4)
        .extrude(grit_height)
        .cut(cutoutArea)
    )


lines = mkLines(grit_width)
rotatedLines = mkLines(grit_length).rotateAboutCenter((0, 0, 1), 90)

# make the border around
border = (
    cq.Workplane("XY")
    .circle(radius + border_width)
    .extrude(border_height)
    .cut(cq.Workplane("XY").circle(radius).extrude(border_height))
)


r = lines.add(rotatedLines).add(border)
# r = lines.add(lines2)
# r = lines.add(lines2).cut(cutoutArea)
show_object(r, "result")
