import cadquery as cq

length = 20
# height = 7
inner_height = 5
inner_width = 7
wall_thickness = 1.5
gap = 3.5

# 
outer_height = inner_height + 2*wall_thickness

base = (cq.Workplane("XY")
        .rect(length, inner_width + 2*wall_thickness)
        .extrude(outer_height)
        .edges("|X and >Z")
        .fillet(outer_height/2 - 10**(-1))
)

topCut = (base
           .faces(">Z")
           .workplane(-2*wall_thickness)
           .rect(length, gap)
           .extrude(2*wall_thickness, False)
)

innerCut = (base
           .faces(">Z")
           .workplane(-outer_height+wall_thickness)
           .rect(40, inner_width)
           .extrude(outer_height - 2*wall_thickness, False)
           .edges("|X and >Z")
           .fillet(inner_width/2 - 10**(-5))
)

middleCut = (base
             .faces("<Z")
             .workplane(-(wall_thickness / 3))
             .rect(length, gap/2)
             .extrude(-wall_thickness, False)
             .edges("|X and <Z")
             .fillet(0.8)
)

test = (base
        .cut(topCut)
        .cut(innerCut)
        .edges("|X")
        .edges("|X and <<Z[-2]")
        # .edges("|X and >Z[2]")
        # .edges("|X and >Z")
        .fillet(0.5)
        .edges("|X and >Z")
        .fillet(1)
        .cut(middleCut)
)

# show_object(base, "base")
# show_object(topCut, "topCut")
# show_object(innerCut, "innerCut")
# show_object(middleCut, "middleCut")
show_object(test, "test")
