import cadquery as cq

# user settings
p_plate_thickness = 3.0
p_plate_radius = 80.0 / 2.0

p_plate_mount_hole = 3.0
p_plate_mount_holespacing = 3.0
p_plate_load_height = 3.0
p_plate_load_length = 47.0

basePlate = (cq.Workplane("XY")
             .circle(p_plate_radius).extrude(p_plate_thickness)
             # .rect(10,10)
)


basePlateMount = (basePlate.workplane(p_plate_thickness/2)
            .rect(p_plate_load_length, 5*p_plate_mount_hole, forConstruction=True)
            .edges(">X and >Z").rect(-(p_plate_mount_hole*2), 5*p_plate_mount_hole, (False, True))
            .extrude(p_plate_load_height)
)

# test = basePlateMount.edges(">X and >Z").rect(-(p_plate_mount_hole*2), 5*p_plate_mount_hole, (False, True)).extrude(10)
