import cadquery as cq

length = 250
height = 150
width = 100

wall_thickness = 4

# side parameters
side_length = length
side_gap_width = 6 # the width of each slit in the side
side_gap_spacing = 4 # spacing between each slit in the side
side_gap_top_spacing = 5 # spacing between top and bottom of gab/slit
side_gap_side_spacing = 5
side_top_height = 20 # the height of the top part (angled part of the side)
side_main_height = height - side_top_height # the height of the top part (angled part of the side)

# try to make profile
def makeProfile(scale = 1.0):
    return (cq.Workplane("XZ")
            .lineTo(0, scale*(height-side_top_height))
            .line(scale*side_top_height, scale*(width/4))
            .line(scale*(width/4), 0)
            .line(0, scale*(-wall_thickness))
            .line(scale*(-width/4+wall_thickness/2), 0)
            .line(scale*(-side_top_height+(wall_thickness/2)),scale*(-width/4+(wall_thickness/2)))
            .lineTo(scale*(wall_thickness), 0)
            .close()
            # .extrude(length/2, both=True)
            # .consolidateWires()
    )

def makeProfileTest(scale = 1):
    return (cq.Workplane("XZ")
            .lineTo(0, (height-side_top_height))
            # .lineTo(0, scale*(height-side_top_height))
            .line(scale*side_top_height, scale*(width/4))
            .line(scale*(width/2), 0)
            .line(scale*(side_top_height), scale*(-width/4))
            .line(0, -(height-side_top_height))
            .close()
            # .line(0, scale*(-wall_thickness))
            # .line(scale*(-width/4+wall_thickness/2), 0)
            # .line(scale*(-side_top_height+(wall_thickness/2)),scale*(-width/4+(wall_thickness/2)))
            # .lineTo(scale*(wall_thickness), 0)
            # .close()
            # .extrude(length/2, both=True)
            .consolidateWires()
    )

sideProfileExtruded = makeProfile().extrude(length/2, both=True)


# side calculations
## calculatee number of slits to make on the main side
## (substract two, as the two on side is different size)
## take the side length, and substract the spacing that is on each side
num_slits = int((side_length-2*side_gap_side_spacing)/(side_gap_width+side_gap_spacing)-2)

# the spacing with rarray, which is the distance from middle
# of one slit, to middle of the next slit
rarray_spacing = side_gap_width + side_gap_spacing

# used to find the spacing for the two smaller slits
side_slit_spacing = (num_slits+1)*(side_gap_width + side_gap_spacing)

sideCutoutSide = (
    cq.Sketch()
    .rarray(rarray_spacing, 1, num_slits, 1)
    .rect(side_gap_width, side_main_height - 2*side_gap_top_spacing)
    .reset()
    .rarray(side_slit_spacing, 1, 2, 1)
    .rect(side_gap_width, side_main_height - 6*side_gap_top_spacing)
    .reset()
    .vertices()
    .fillet(side_gap_width / 2.5)
)

# calculate length and spacing for top slits
top_slit_length = (side_length - 2*side_gap_side_spacing - 4 * side_gap_spacing)/2
top_angle_rarray_spacing = top_slit_length + 4 * side_gap_spacing

sideCutoutTop = (
    cq.Sketch()
    .rarray(top_angle_rarray_spacing, side_gap_spacing + side_gap_width, 2, 2)
    .rect(top_slit_length, side_gap_width)
    .reset()
    .vertices()
    .fillet(side_gap_width / 2.5)
)

# make side
side = (
    # get the vertical large face
    sideProfileExtruded.faces("<X")
    .workplane(centerOption="CenterOfMass")
    .placeSketch(sideCutoutSide)
    .cutThruAll()

    # gets the sloped face
    .faces("<<Z[3]")
    .workplane(centerOption="CenterOfMass")
    .placeSketch(sideCutoutTop)
    .cutThruAll()

    # get the top face
    .faces(">Z")
    .workplane(centerOption="CenterOfMass")
    .transformed(rotate=cq.Vector(0, 0, 90))
    .placeSketch(sideCutoutTop)
    .cutThruAll()
)

# testProfile = profile.edges(">X").tag("joinpoint")
# testProfile2 = profile.mirrorY().edges("<X").tag("joinpoint")

# bridge = (
#     cq.Assembly()
#     .add(makeProfile(), name="right")
#     .add(makeProfile(), name="left")
# )



# brigdeCon = bridge.constrain("right@edges@>X", "left?joinpoint", "Plane")
# brigdeCon = bridge.constrain("right@edges@>X", "left@edges@>X", "Plane")
# testProfile = profile

endTest = (
    # profile.mirror(mirrorPlane="XZ")
    # profile.mirrorY()
    # profile

)

test = (
    makeProfile()
    .translate((-width/2+wall_thickness, 0, 0))
    # .mirror(mirrorPlane="YZ")
    
)

p1 = makeProfile()
p1e = p1.extrude(1)
p1f = p1e.mirror(p1e.faces(">X"), union=True)

# p2 = makeProfile(0.80)
# p2e = p2.extrude(1)
# p2f = p2e.mirror(p2e.faces(">X"), union=True).translate(((width)/12, 20, 20))
# # p2f = p2f.faces("<Z and <Y")
# p2Trans = p2.translate((10,10,10))

# p3f = p1f.add(p2f)

# tapered
baseSide = makeProfileTest()
baseSideSmall = (
    makeProfileTest(0.75)
    .translate((width/8, 0, 0))
)

test = (
    # makeProfileTest(0.7)
    baseSide
    .add(baseSideSmall)
    .wires()
    .loft()
    # .faces(">Y")
    # .edges("not <Z")
    # .extrude(10, taper=45)
    # .edges(">Y and not <Z")
    # .edges("<Z and not(<Z)")
)

# show_object(test, "test1")
show_object(makeProfileTest(), "test3")
show_object(baseSideSmall, "test2")
# show_object(makeProfileTest(0.75), "test3")
# show_object(makeProfileTest().extrude(20, taper=45), "test1")

# # r4 = cq.Workplane().placeSketch(s, s.moved(cq.Location(cq.Vector(0, 0, height)))).loft()
# r4 = cq.Workplane().placeSketch(s, s.moved(cq.Location(cq.Vector(0, 0, height)))).loft()
