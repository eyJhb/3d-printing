import cadquery as cq

width = 5
depth = 2
length = 200
fontsize = 20


def makeSignWithText(text):
    text = text.upper()

    stick = cq.Workplane("XY").box(length, width, depth, centered=False)

    text = (
        cq.Workplane("XY")
        .text(text, fontsize, depth, halign="right", valign="bottom")
        .rotateAboutCenter((0, 1, 0), 180)
        .translate((length + 1, width, 0))
    )

    return stick.add(text)


def makeWriteableSign(height: float, width: float, depth: float):
    ratio = 0.85
    stick = (cq.Workplane("XY")
             .lineTo(height-height*ratio, width/2)
             .lineTo(height, width/2)
             .lineTo(height, -width/2)
             .lineTo(height-height*ratio, -width/2)
             .lineTo(0, 0)
             .close()
             .extrude(depth)
             # .edges(">Z or <Z").fillet(depth/2-0.0001)
    )
    return stick


# test = makeSignWithText("GARLIC")
test2 = makeWriteableSign(100.0, 10.0, 1.5)
