import cadquery as cq

outer_inner_radius = 88.9/2
inner_outer_radius = outer_inner_radius/3
height = 15
wall_thickness = 1.5


base = (cq.Workplane("XZ")
        .move(outer_inner_radius + wall_thickness)
        .line(0, height + wall_thickness)
        .line(-(outer_inner_radius - inner_outer_radius)-2*wall_thickness, 0)
        .line(0, -(height+wall_thickness))
        .line(wall_thickness, 0)
        .line(0, height)
        .line(outer_inner_radius-inner_outer_radius, 0)
        .line(0, -height)
        .close()
        # .line(-o 
        .revolve()
        # .consolidateWires()
)

# testCalc = (outer_inner_radius-inner_outer_radius)-1*wall_thickness
# testCalc = (outer_inner_radius+wall_thickness)/2
# testCalc = outer_inner_radius/2
testCalc = (outer_inner_radius-inner_outer_radius)/2+inner_outer_radius

test = (cq.Workplane("XY")
        .polarArray(testCalc, 0, 360, 8)
        .circle((outer_inner_radius-inner_outer_radius)*0.25)
        .extrude(50)
)

r = (base.cut(test)
     .faces("<Z")
     .chamfer(0.5)
)

show_object(r)


