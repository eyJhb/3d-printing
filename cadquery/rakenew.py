import cadquery as cq

bladeLength = 60
bladeHeight = 30
bladeRadius = bladeLength

handleLength = 125
handleWidth = 50/2

depth = 2

# r = cq.Workplane("front").hLine(1).vLine(10).hLine(4).hLineTo(0.0).close()

r = (cq.Workplane("front").radiusArc((bladeLength, bladeHeight), bladeRadius).vLine(-20).radiusArc((0,0), -bladeRadius).close()
     .vertices(">Y and >X").rect(handleWidth, -handleLength, False)
     .extrude(depth)
)


base = cq.Workplane("XY").circle(50)

# test = cq.Workplane("XY").radiusArc((bladeLength, bladeLength/1.5), bladeLength)
# test = test.vLine(bladeLength)

# test  = cq.Workplane("XY").radiusArc((bladeLength, bladeLength/1.5), bladeLength)
# test2 = cq.Workplane("XY").radiusArc((bladeLength, bladeLength), bladeLength)

# box = test2.vertices(">Y").rect(handleWidth, -handleLength, False)

# combined = box.add(test).add(test2).extrude(3)


# show_object(combined)
