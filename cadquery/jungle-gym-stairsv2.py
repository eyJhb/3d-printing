import cadquery as cq

# stair triangle at posts
#       65.5    90*
#  40*  ---------
#       \       |
#        \      |
#         \     | 55
#          \    |
#           \   |
#       85   \  |
#             \ |
#              \|
#               50*


wall_thickness = 2
length = 85
depth = 10

height = 55 / 4

test = (
    cq.Workplane("YZ")
    .lineTo(0, 12)
    .lineTo(4, 0)
    .lineTo(20, 0)
    .lineTo(20, -wall_thickness)
    .lineTo(0, -wall_thickness)
    .close()
    .extrude(length)
    .edges(">Y and |Z")
    .fillet(1)
    .edges("|Y and <Z")
    # .fillet(2)
    # .consolidateWires()
)
