num_cylinders = 30;
radius = 10;
height = 20;

for(i = [0:num_cylinders-1]) {
    rotate(i * 360/num_cylinders)
    translate([100,100,0])
    cylinder(r=radius,h=height);
}