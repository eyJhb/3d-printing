$fn=50;

show_box = 1;
show_lid = 1;

// parameters for external dimensions for enclosure
length = 50;
width = 35;
height = 18;

// additional parameters
corner_radius = 5; // higher = more rounded
wall_thickness = 2; // total thickness of each wall
post_diameter = 5.5; // support post diameter
hole_diameter = 2.5; // size for screws

// lid parameters
lid_thickness = 2;
lid_lip = wall_thickness - 1;
lid_tolerance = 0.1;

// lid countersink
countersink = 1;
cs_diameter = hole_diameter/2 + 1;
cs_depth = lid_thickness/2;


module posts(x,y,z,h,r,half=0) {
    if(half == 0) {
        translate([x,y,z]){
            cylinder(r=r, h=h);
        }
    }
    translate([-x,y,z]){
        cylinder(r=r, h=h);
    }
    if(half == 0) {
        translate([-x,-y,z]){
            cylinder(r=r, h=h);
        }
    }
    translate([x,-y,z]){
        cylinder(r=r, h=h);
    }
}

// box
if(show_box) {
    difference() {
        // box
        hull(){
            posts(
                x=(width/2 - corner_radius),
                y=(length/2 - corner_radius),
                z=0,
                h=height,
                r=corner_radius
           );
        }
        
        // hollow
        hull(){
            posts(
                x=(width/2 - corner_radius - wall_thickness),
                y=(length/2 - corner_radius - wall_thickness),
                z=wall_thickness,
                h=height,
                r=corner_radius
           );
        }
    
        // lip
        hull(){
            posts(
                x=(width/2 - corner_radius - lid_lip),
                y=(length/2 - corner_radius - lid_lip),
                z=(height - lid_thickness),
                h=lid_thickness+1, // add one to ensure we clear the box
                r=corner_radius
           );
        }
    }
    
    // make support posts
    difference() {
        posts(
            x=(width/2 - wall_thickness - post_diameter/2),
            y=(length/2 - wall_thickness - post_diameter/2),
            z=(wall_thickness - 0.5),
            h=height - wall_thickness - lid_thickness + 0.4,
            r=post_diameter/2,
            half=1
        );
        
        // make holes in support posts
        posts(
            x=(width/2 - wall_thickness - post_diameter/2),
            y=(length/2 - wall_thickness - post_diameter/2),
            z=wall_thickness,
            h=height - wall_thickness - lid_thickness + 0.5,
            r=hole_diameter/2,
            half=1
        );
    }
}

// lid
if(show_lid) {
  difference() {
      hull() {
          posts(
              x=(width/2 - wall_thickness/2 - corner_radius - lid_tolerance),
              y=(length/2 - wall_thickness/2 - corner_radius - lid_tolerance),
              z=height - lid_thickness + 5, // TODO(eyJhb): remove + 5
              h=lid_thickness,
              r=corner_radius
          );
      }
  
      // holes for support
      posts(
          x=(width/2 - wall_thickness - post_diameter/2),
          y=(length/2 - wall_thickness - post_diameter/2),
          z=height - lid_thickness + 4.5, // TODO(eyJhb): remove + 5
          h=wall_thickness - lid_thickness + 2,
          r=hole_diameter/2 + 0.1,
          half=1
      );
  
      // downhole
      if(countersink) {
        posts(
            x=(width/2 - wall_thickness - post_diameter/2),
            y=(length/2 - wall_thickness - post_diameter/2),
            z=height - cs_depth + 5, // TODO(eyJhb): remove + 5
            h=10,
            r=cs_diameter,
            half=1
        );
      }
  }
}